import json, requests
from pymongo import MongoClient

params = {'access_token':"EAAKCGRWxULABAJGkY8nm2QDyetI4sOZABAqJWNvvPn8ccjdWomdZCUEwccHTCZB86Ef6Pz4hyemsAhvB1MoqEIDTnF7VdIAoZBQuxTQOJZAX5ef7MZAvUjNL1vizP90M8XYwqne7wnZCC8zXkZCoc0WqgFDO8UqfErBKWD0sL4fyMgZDZD"}
 
page_url = 'https://graph.facebook.com/v2.11/Google/feed?fields=id,message,reactions,share,from,caption,created_time,likes.summary(true)'
comments_url = 'https://graph.faceboom.com/v2.11/{post_id}/comments?filter=stream&llimit=100'

posts = requests.get(page_url, params=params)
posts = posts.json()
collection_comments = []
posts_data = []
comments_data = []

while True:
    try:
        print('Posts:',posts)
        for element in posts['data']:
            print('element:',element)
            collection_posts.insert(element)
            this_comment_url = comments_url.replace("{post_id}", element['id'])
            comments = requests.get(this_comment_url, params=params).json()
            while('paging' in comments and 'cursors' in comments['paging'] and 'after' in comments['paging']['cursors']):
                for comment in comments['data']:
                    comment['post_id'] = element['id']
                    collection_comments.insert(comment)
                    print(comment)
                comments = requests.get(this_comment_url + '&after=' + comments['paging']['cursors']['after'], params=params).json()
        
        posts = requests.get(posts['paging']['next']).json()

        for doc in collection_posts.find({}):
            try:
                posts_data.append((doc['message'],doc['created_time'],doc['likes']['summary']['total_count'],doc['shares']['count'],doc['id']))
            except:
                print('no message')
                pass
        for comment in collection_comments.find({}):
            try:
                comments_data.append((comment['message'], comment['created_time'],comment['post_id']))
            except:
                pass
    except KeyError:
        print('error')
        break
    
