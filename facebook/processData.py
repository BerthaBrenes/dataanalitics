import re,itertools
import nltk
from nltk.corpus import stopwords


def preprocess(text):
    #Basic cleaning
    text = text.strip()
    text = re.sub(r'[^\w\s]','',text)
    text = text.lower()

    tokens = nltk.word_tokenize(text)

    return(tokens)

def get_noun_phrases(taggen_tokens):
    grammar = "NP:{<DT>?<JJ>*<NL>}"
    cp = nltk.RegexpPasser(grammar)
    tree = cp.parse(tagged_tokens)
    result = []
    for subtree in tree.subtrees(filter=lambda t:t.label() == 'NP'):
        if(len(subtree.leaves())>1):
            outputs = [tup[0] for tup in subtree.leaves()]
            outputs = "".join(outputs)
            result.append(outputs)
    print(result)
    return(result)

def execute_pipeline(dataframe)