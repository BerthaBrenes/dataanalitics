from pylab import *
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

matplotlib.rcParams.update({'font.size': 18, 'font.family': 'STIXGeneral', 'mathtext.fontset': 'stix'})
x = np.linspace(0,5,10)
y = x ** 2

"""x = np.linspace(0,5,10)
y = x ** 2
figure()
subplot(1,2,1)

plot(x, y, 'r--')
xlabel('x')
ylabel('y')
title('prueba')

subplot(1,2,2)
plot(y, x, 'g*-')#r es red y -- son la forma en la que se va a mostrar igual que g y *-

xlabel('x')
ylabel('y')
title('prueba')"""

fig, ax = plt.subplots()

ax.plot(x, x**2, label="y = x**2")
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_title('title')



show()
