import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import matplotlib
sns.set()

plt.close('all')
df = pd.read_csv("/home/ordipret2/BerthaFiles/proyectoData/Universities/resultsAddLang.csv")
df.head()
#Cuenta la cantidad de datos que hay en una dataFrame
df.describe()
#Vamos agrupar la tabla por la columna fromFile
df_by_year = df.groupby('fromFile')
type(df_by_year)
#Ahora como parte importante cuenta la cantidad de Datos de acuerdo a la columna 'fromFile'
df_by_year.describe().head()
#Separo la dataFrame de acuerdo a cada Universidad y lo hago una lista con toda la informacion separada por '/'
CATIE = list(df_by_year)[0]
Earth = list(df_by_year)[1]
TEC = list(df_by_year)[2]
UCR = list(df_by_year)[3]
UNA = list(df_by_year)[4]
#Ver sin repetirse la cantidad de lenguajes que hay en la lista de la universidad que me interesa buscando la informacion la colunma de lenguaje
languages_name = sorted(UNA[1]['Lenguaje'].unique())
print(languages_name)
#Agrupar todo de acuerdo a cuantos hay por cada lenguaje
group_by_lang = UNA[1].groupby(by=['Lenguaje'])
#Cuenta y muesta en forma de tabla con respecto a todos los otros archivos
lang_count = group_by_lang.count()
print(lang_count)
#Me da el numero de veces que aparece Lenuaje de acuerdo al Titulo
number_file = lang_count['Titulo']
print(number_file)
#Con los datos se configura el matplot para potear
matplotlib.rcParams.update({'font.size': 18, 'font.family': 'STIXGeneral', 'mathtext.fontset': 'stix'})
#Como los lenguajes no son datos numericos, contamos la cantidad que tenemos para hacer las columnas
index = np.arange(len(languages_name))
#Ploteo las barras 'y' con respecto a la cantidad de idiomas y la cantidad de files que hay de cada uno
plt.bar(index, number_file)
#seteo el nombre de X y Y
plt.xlabel('Language', fontsize=12)
plt.ylabel('Numbers File', fontsize=12)
#Coloco el nombre correspondiente a las barras 'x' con los idiomas
plt.xticks(index,languages_name, fontsize=12, rotation=90)
#Titulo de la grafica y show para mostrar la grafica
plt.title('File in UNA according to language')
plt.show()