from selenium import webdriver
import selenium
import pandas as pd 
import time 

driver = webdriver.Firefox(executable_path=r'/usr/local/bin/geckodriver-v0.24.0-linux64/geckodriver')
papers = pd.DataFrame(columns = ['Titulo','link','autor','Periodo','formato'])
time.sleep(4)

def scraper(u):
    print(u+1)
    n = '/html/body/table[2]/tbody/tr[1]/td[2]/table/tbody/tr[{}]/td[2]/a'.format(u+1)
    print(n)
    nombre = driver.find_element_by_xpath(n)
    print(nombre.text)
    p = '/html/body/table[2]/tbody/tr[1]/td[2]/table/tbody/tr[{}]/td[1]'.format(u+1)
    periodo = ''
    try:
        period = driver.find_element_by_xpath(p)
        periodo = period.text
        print(period.text)
    except selenium.common.exceptions.NoSuchElementException:
        pass
    a = '/html/body/table[2]/tbody/tr[1]/td[2]/table/tbody/tr[{}]/td[3]/em/a'.format(u+1)
    autor = driver.find_element_by_xpath(a)
    print(autor.text)
    l = '/html/body/table[2]/tbody/tr[1]/td[2]/table/tbody/tr[{}]/td[2]/a'.format(u+1)
    link = driver.find_element_by_xpath(l)
    print(link.get_attribute('href'))
    papers.loc[len(papers)] = [nombre.text,link.get_attribute('href'),autor.text,periodo,'Articulo']

for j in range(4):
    url = 'http://repositorio.sibdi.ucr.ac.cr:8080/jspui/simple-search?query=agricultura&sort_by=0&order=DESC&rpp=10&etal=0&start={}'.format(j*10)
    driver.get(url)
    for i in range(10):
        time.sleep(1)
        try:
            scraper(i+1)
        except selenium.common.exceptions.NoSuchElementException:
            pass

export_csv = papers.to_csv (r'/home/ordipret2/BerthaFiles/proyectoData/UCR3Results.csv', index = None, header=True) 