from selenium import webdriver
import pandas as pd 
import time 

driver = webdriver.Firefox(executable_path=r'/usr/local/bin/geckodriver-v0.24.0-linux64/geckodriver')
driver.get('https://earthac.sharepoint.com/biblioteca/SiteAssets/EBSCO.aspx')

text_area_email = driver.find_element_by_id('i0116')
text_area_email.send_keys("XXXXX")

nextButton = driver.find_elements_by_xpath('//*[@id="idSIButton9"]')[0]
nextButton.click()
time.sleep(1)
text_area_password = driver.find_element_by_id('i0118')
text_area_password.send_keys("XXXX")
time.sleep(1)

nextButton2 = driver.find_element_by_css_selector('#idSIButton9')
nextButton2.click()
time.sleep(1)

nextButton3 = driver.find_elements_by_xpath('//*[@id="idSIButton9"]')[0]
nextButton3.click()
time.sleep(2)
window_login = driver.window_handles[0]

nextButton4 = driver.find_elements_by_xpath('/html/body/form/div[12]/div/div[2]/div[2]/div[3]/table/tbody/tr/td/div/div/div/div/div[1]/table/tbody/tr/td[2]/table/tbody/tr/td/div/strong/a')[0]
nextButton4.click()

time.sleep(2)
window_search = driver.window_handles[1]
driver.switch_to_window(window_search)

time.sleep(4)

text_area_search = driver.find_element_by_id('SearchTerm1')
text_area_search.send_keys('Agroecologia')
nextButton5 = driver.find_elements_by_xpath('//*[@id="SearchButton"]')[0]
nextButton5.click()

time.sleep(4)

papers = pd.DataFrame(columns = ['Date','Link']) 
link = []
print(len(papers))
for i in range(129):
    time.sleep(3)
    nextButton6 = driver.find_elements_by_xpath('//*[@id="ctl00_ctl00_MainContentArea_MainContentArea_bottomMultiPage_lnkNext"]')[0]
    nextButton6.click()
while(len(papers) < 9 ):
    for i in range(30):
        ide = '//*[@id="Result_{}"]'.format(i+1291)
        if((i+1291)%10 ==0):
            print(ide)
            paper_name = driver.find_elements_by_xpath(ide)[0]
            name = paper_name.text
            print(name)
            time.sleep(3)
            li = '//*[@id="pdfft{}"]'.format(i+1291)
            print(li)
            paper_link = []
            if(driver.find_elements_by_xpath(li)):
                linkButton5 = driver.find_elements_by_xpath(li)[0]
                linkButton5.click()
                time.sleep(4)
                paper_link = driver.find_elements_by_xpath('//*[@id="downloadLink"]')[0]
                link = paper_link.get_attribute('href')
                time.sleep(3) 
                backButton6 = driver.find_elements_by_xpath('//*[@id="ctl00_ToolbarArea_returnLink"]')[0]
                backButton6.click()
            time.sleep(5)
            nextButton6 = driver.find_elements_by_xpath('//*[@id="ctl00_ctl00_MainContentArea_MainContentArea_bottomMultiPage_lnkNext"]')[0]
            nextButton6.click()
        else:
            time.sleep(3)
            print(ide)
            paper_name = driver.find_elements_by_xpath(ide)[0]
            name = paper_name.text
            print(name)
            time.sleep(3)
            li = '//*[@id="pdfft{}"]'.format(i+1291)
            print(li)
            paper_link = []
            if(driver.find_elements_by_xpath(li)):
                linkButton5 = driver.find_elements_by_xpath(li)[0]
                linkButton5.click()
                time.sleep(4)
                paper_link = driver.find_elements_by_xpath('//*[@id="downloadLink"]')[0]
                link = paper_link.get_attribute('href')
                time.sleep(3) 
                backButton6 = driver.find_elements_by_xpath('//*[@id="ctl00_ToolbarArea_returnLink"]')[0]
                backButton6.click()
        papers.loc[len(papers)] = [name, link]
    

export_csv = papers.to_csv (r'/home/ordipret2/BerthaFiles/scraper/dataframe43.csv', index = None, header=True) 
