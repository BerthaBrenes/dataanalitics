from selenium import webdriver
import selenium
import pandas as pd 
import time 

driver = webdriver.Firefox(executable_path=r'/usr/local/bin/geckodriver-v0.24.0-linux64/geckodriver')

driver.get('https://primo-tc-na01.hosted.exlibrisgroup.com/primo-explore/search?query=any,contains,agroecologia&tab=itcr_tab&search_scope=itcr_aleph&vid=ITCR&lang=es_CR&offset=0')
papers = pd.DataFrame(columns = ['Periodo','Titulo','autor','formato','link'])
time.sleep(4)

def scraper(u):
    print(u+1)
    n = '/html/body/primo-explore/div/prm-explore-main/ui-view/prm-search/div/md-content/div[2]/prm-search-result-list/div/div[2]/div/div[{}]/prm-brief-result-container/div[1]/div[3]/prm-brief-result/h3/a/span/prm-highlight/span'.format(u+1)
    print(n)
    nombre = driver.find_element_by_xpath(n)
    print(nombre.text)
    p = '/html/body/primo-explore/div/prm-explore-main/ui-view/prm-search/div/md-content/div[2]/prm-search-result-list/div/div[2]/div/div[{}]/prm-brief-result-container/div[1]/div[3]/prm-brief-result/div[2]/span/span[2]/prm-highlight/span'.format(u+1)
    periodo = ''
    try:
        period = driver.find_element_by_xpath(p)
        periodo = period.text
        print(period.text)
    except selenium.common.exceptions.NoSuchElementException:
        pass
    a = '/html/body/primo-explore/div/prm-explore-main/ui-view/prm-search/div/md-content/div[2]/prm-search-result-list/div/div[2]/div/div[{}]/prm-brief-result-container/div[1]/div[3]/prm-brief-result/div[1]/span/span[2]/prm-highlight/span'.format(u+1)
    autor = driver.find_element_by_xpath(a)
    print(autor.text)
    f = '/html/body/primo-explore/div/prm-explore-main/ui-view/prm-search/div/md-content/div[2]/prm-search-result-list/div/div[2]/div/div[{}]/prm-brief-result-container/div[1]/div[3]/div[1]/span'.format(u+1)
    formato = driver.find_element_by_xpath(f)
    print(formato.text)
    time.sleep(1)
    m = '/html/body/primo-explore/div[1]/prm-explore-main/ui-view/prm-search/div/md-content/div[2]/prm-search-result-list/div/div[2]/div/div[{}]/prm-brief-result-container/div[2]/button[3]/prm-icon[2]/md-icon'.format(u+1)
    menu_botton = driver.find_element_by_xpath(m)
    menu_botton.click()
    time.sleep(1)
    lb = '/html/body/primo-explore/div/prm-explore-main/ui-view/prm-search/div/md-content/div[2]/prm-search-result-list/div/div[2]/div/div[{}]/prm-brief-result-container/div[3]/md-content/prm-action-list/md-nav-bar/div/nav/ul/div[2]/li[7]/button/span/div/span'.format(u+1)
    link_button = driver.find_element_by_xpath(lb)    
    link_button.click()
    time.sleep(1)
    li = '/html/body/primo-explore/div[1]/prm-explore-main/ui-view/prm-search/div/md-content/div[2]/prm-search-result-list/div/div[2]/div/div[{}]/prm-brief-result-container/div[3]/md-content/prm-action-list/prm-action-container/prm-permalink/div/md-content/div/div[3]/div'.format(u+1)
    l = driver.find_element_by_xpath(li)
    print(l)
    cl ='/html/body/primo-explore/div/prm-explore-main/ui-view/prm-search/div/md-content/div[2]/prm-search-result-list/div/div[2]/div/div[{}]/prm-brief-result-container/div[2]/button[3]/prm-icon[2]/md-icon'.format(u+1)
    close = driver.find_element_by_xpath(cl)
    close.click()
    time.sleep(2)

    papers.loc[len(papers)] = [periodo,nombre.text,autor.text,formato.text,l]
    
for i in range(69):
    print(i+1)
    if((((i+1)%10)==1)):
        if((i+1)==1):
            scraper(i)
        else:
            
            bn = '/html/body/primo-explore/div/prm-explore-main/ui-view/prm-search/div/md-content/div[2]/prm-search-result-list/div/div[2]/div/div[{}]/button'.format(i+1)
            nextButton = driver.find_element_by_xpath(bn)
            nextButton.click()
            print("entramos a una nueva pagina")
            time.sleep(4)
            scraper(i)
    else:
        scraper(i)

export_csv = papers.to_csv (r'/home/ordipret2/BerthaFiles/proyectoData/TECSIBUT.csv', index = None, header=True) 