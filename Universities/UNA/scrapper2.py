from selenium import webdriver
import selenium
import pandas as pd 
import time 

driver = webdriver.Firefox(executable_path=r'/usr/local/bin/geckodriver-v0.24.0-linux64/geckodriver')
driver.get('http://www.opac.una.ac.cr/F/GS3F19FIG8FIT4M48HDCQ3NG8B6ARVJYBUQBQ36XR54T7YF8PB-57699?func=full-set-set&set_number=000398&set_entry=000162&format=999')

papers = pd.DataFrame(columns = ['Periodo','Titulo','autor','formato'])

def scrapper():
    t = '/html/body/table[6]/tbody/tr[5]/td[2]/a'
    name = driver.find_element_by_xpath(t)
    print(name.text)
    f = '/html/body/table[6]/tbody/tr[2]/td[2]'
    formato = driver.find_element_by_xpath(f)
    print(formato.text)
    a = '/html/body/table[6]/tbody/tr[4]/td[2]'
    autor = driver.find_element_by_xpath(a)
    print(autor.text)
    periodo = ''
    try:
        peri = driver.find_element_by_xpath('/html/body/table[6]/tbody/tr[11]/td[13]/a')
        periodo = peri.text
        print(periodo)
    except selenium.common.exceptions.NoSuchElementException:
        try:
            peri = driver.find_element_by_xpath('/html/body/table[6]/tbody/tr[12]/td[9]/a')
            periodo = peri.text
            print(periodo)
        except selenium.common.exceptions.NoSuchElementException:
            print('pass')
            try:
                peri = driver.find_element_by_xpath('/html/body/table[6]/tbody/tr[14]/td[15]/a')
                periodo = peri.text
                print(periodo)
            except selenium.common.exceptions.NoSuchElementException:
                print('pass')
                pass
    papers.loc[len(papers)] = [periodo,name.text,autor.text,formato.text]     
for i in range(300):
    print(i)
    try:
        scrapper()
    except selenium.common.exceptions.NoSuchElementException:
        print('pass')
        pass
    time.sleep(1)
    next_button = driver.find_element_by_xpath('/html/body/table[5]/tbody/tr/td[2]/a[2]')
    next_button.click()

export_csv = papers.to_csv (r'/home/ordipret2/BerthaFiles/proyectoData/Universities/UNA/try1.csv', index = None, header=True) 