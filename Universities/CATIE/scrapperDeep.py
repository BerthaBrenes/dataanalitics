from selenium import webdriver
import pandas as pd 
import time 
import selenium


driver = webdriver.Firefox(executable_path=r'/usr/local/bin/geckodriver-v0.24.0-linux64/geckodriver')
time.sleep(2)
papers = pd.DataFrame(columns = ['Titulo','link','autor','Periodo','formato'])

def scrapper():
    n = '/html/body/div[3]/div[1]/div/div[1]/div[1]/div/div/h2'
    name = driver.find_element_by_xpath(n)
    print(name.text)
    p = '/html/body/div[3]/div[1]/div/div[1]/div[1]/div/div/div/div[1]/div[2]'
    period = driver.find_element_by_xpath(p)
    print(period.text)
    l1 = '/html/body/div[3]/div[1]/div/div[1]/div[1]/div/div/div/div[1]/div[3]/a'
    l2 = '/html/body/div[3]/div[1]/div/div[1]/div[1]/div/div/div/div[1]/div[4]/a'
    link = ''
    try:
        ln = driver.find_element_by_xpath(l1)
        link = ln.get_attribute('href')
        print('1',link )
    except selenium.common.exceptions.NoSuchElementException:
        try:
            ln = driver.find_element_by_xpath(l2)
            link = ln.get_attribute('href')
            print('2',link )
        except selenium.common.exceptions.NoSuchElementException:
            pass
    f1 = '/html/body/div[3]/div[1]/div/div[1]/div[1]/div/div/div/div[2]/div[2]/ul/li/a'
    f2 = '/html/body/div[3]/div[1]/div/div[1]/div[1]/div/div/div/div[2]/div[3]/ul/li/a'
    formato = ''
    try:
        fn = driver.find_element_by_xpath(f1)
        formato = fn.text
        print('1',formato)
    except selenium.common.exceptions.NoSuchElementException:
        try:
            fn = driver.find_element_by_xpath(f2)
            formato = fn.text
            print('2',formato)
        except selenium.common.exceptions.NoSuchElementException:
            pass
    a = '/html/body/div[3]/div[1]/div/div[1]/div[1]/div/div/div/div[1]/div[3]'
    autor = 'autor desconocido'
    try:
        an = driver.find_element_by_xpath(a)
        autor = an.text
        print(autor)
    except selenium.common.exceptions.NoSuchElementException:
        pass
    
    papers.loc[len(papers)] = [name.text,link,autor,period.text,formato]

for p in range(46):
    url = 'http://repositorio.bibliotecaorton.catie.ac.cr/discover?rpp=10&etal=0&query=agroecologia&scope=/&group_by=none&page={}'.format(p+1)
    driver.get(url)
    print('p',p)
    for i in range(9):
        print('i',i)
        time.sleep(2)
        q = '/html/body/div[3]/div[1]/div/div[1]/div[1]/div/div[3]/div[{}]/div[2]/a/h4'.format(i+2)
        inside_button = driver.find_element_by_xpath(q)
        inside_button.click()
        time.sleep(2)
        try:
            scrapper()
        except selenium.common.exceptions.NoSuchElementException:
            pass
        time.sleep(3)
        driver.execute_script("window.history.go(-1)")
        
export_csv = papers.to_csv (r'/home/ordipret2/BerthaFiles/proyectoData/CATIEResults.csv', index = None, header=True)