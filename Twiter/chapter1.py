import json
import twitter
from urllib.parse import unquote
from prettytable import PrettyTable
import csv

CONSUMER_KEY = 'drHEyh32n3qNDMxM9DXcSuj05'
CONSUMER_SECRET = 'AIyuk5optwAScjSYK6xmoFjFxZZM5aD8Nkvy3p2XyEXjpVXutq'
OAUTH_TOKEN = '1034547400671682560-rXx6UokNaqcO8XQw0B5BBSkkvaSzlG'
OAUTH_TOKEN_SECRET = 'pdLkWUbfSIrJxjBMA22hhuPgJx5NXg9Ycu4iV4yi7jnkR'
q = 'agricultura Organica' 
f = open("twiter3.json","w+")
count = 100

auth = twitter.oauth.OAuth(OAUTH_TOKEN, OAUTH_TOKEN_SECRET, CONSUMER_KEY, CONSUMER_SECRET)

twitter_api = twitter.Twitter(auth=auth)

print(twitter_api)
search_results = twitter_api.search.tweets(q=q, count=count)

statuses = search_results['statuses']


# Iterate through 5 more batches of results by following the cursor
for _ in range(5):
    print('Length of statuses', len(statuses))
    try:
        next_results = search_results['search_metadata']['next_results']
    except KeyError as e: # No more results when next_results doesn't exist
        break
        
    # Create a dictionary from next_results, which has the following form:
    # ?max_id=847960489447628799&q=%23RIPSelena&count=100&include_entities=1
    kwargs = dict([ kv.split('=') for kv in unquote(next_results[1:]).split("&") ])
    
    search_results = twitter_api.search.tweets(**kwargs)
    statuses += search_results['statuses']

# Show one sample search result by slicing the list...
#print(json.dumps(statuses[0], indent=1))
f.write(str(statuses[0]))

for i in range(10):
    print()
    print(statuses[i]['text'])
    print('Favorites: ', statuses[i]['favorite_count'])
    print('Retweets: ', statuses[i]['retweet_count'])
retweets = [
            # Store out a tuple of these three values ...
            (status['retweet_count'], 
             status['retweeted_status']['user']['screen_name'],
             status['retweeted_status']['id'],
             status['text']) 
            
            # ... for each status ...
            for status in statuses 
            
            # ... so long as the status meets this condition.
                if 'retweeted_status' in status.keys()
           ]

# Slice off the first 5 from the sorted results and display each item in the tuple

pt = PrettyTable(field_names=['Count', 'Screen Name', 'Tweet ID', 'Text'])
[ pt.add_row(row) for row in sorted(retweets, reverse=True)[:5] ]
pt.max_width['Text'] = 50
pt.align= 'l'
print(pt)