from pandas import DataFrame
import pandas as pd 
import matplotlib.pyplot as plt
import numpy as np
import matplotlib

plt.close('all')
data = pd.read_csv("/home/ordipret2/BerthaFiles/proyectoData/Universities/resultsAddLang.csv")


data.head(2)
#Ver sin repetirse la cantidad de anos que hay en el documento
publication_number = data['Periodo'].unique()
print(publication_number)
#Agrupar todo de acuerdo a cuantos hay por cada periodo de ano
group_by_date = data.groupby(by=['Periodo'])
#Cuenta y muesta en forma de tabla con respecto a todos los otros archivos
car_data_count = group_by_date.count()
print(car_data_count)
#Me da el numero de veces que aparece Lenuaje de acuerdo al Titulo
access_number = car_data_count['formato']
print(len(access_number))
print(len(publication_number))

#Plotear en matplot con los siguientes parametros iniciales
matplotlib.rcParams.update({'font.size': 18, 'font.family': 'STIXGeneral', 'mathtext.fontset': 'stix'})
index = np.arange(len(publication_number))
print()
plt.bar(index, access_number)
plt.xlabel('Year', fontsize=12)
plt.ylabel('Numbers', fontsize=12)
plt.xticks(index,publication_number, fontsize=12, rotation=90)
plt.title('Paper according with the year')
plt.show()