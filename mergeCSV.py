import pandas as pd

#Data Us
data1 = pd.read_csv("/home/ordipret2/BerthaFiles/proyectoData/Universities/UCR/resultsMergeYearsUCR.csv")
data2 = pd.read_csv("/home/ordipret2/BerthaFiles/proyectoData/Universities/UNA/resultsByPeriodoUNA.csv")
data3 = pd.read_csv("/home/ordipret2/BerthaFiles/proyectoData/Universities/TEC/resultsByPeriodTEC.csv")
data4 = pd.read_csv("/home/ordipret2/BerthaFiles/proyectoData/Universities/Earth/resultsByPeriodoEarthF.csv")
data5 = pd.read_csv("/home/ordipret2/BerthaFiles/proyectoData/Universities/CATIE/resultsByPeriodoCATIEF.csv")

"""#Data Earth
data = []
for i in range(43):
    path = "/home/ordipret2/BerthaFiles/proyectoData/resultsMergeEarth{}.csv".format(i)
    print(path)
    try:
        data.append(pd.read_csv(path))
    except FileNotFoundError:
        pass
"""


df_row = pd.concat([data1,data2,data3,data4,data5],ignore_index=True,sort=True)
#export_csv = df_row.to_csv (r'/home/ordipret2/BerthaFiles/proyectoData/resultsMergeEarthP.csv', index = None, header=True) 
duple = df_row.drop_duplicates(['Titulo'], keep='last')
sort_by_Titulo = duple.sort_values('Titulo')
sort=True
print(sort_by_Titulo)

export_csv = sort_by_Titulo.to_csv (r'/home/ordipret2/BerthaFiles/proyectoData/resultsMergeAllU.csv', index = None, header=True)